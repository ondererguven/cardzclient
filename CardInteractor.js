'use strict';

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import Card from './models/card';

export default class CardInteractor extends Component<{}> {
  constructor(props) {
    super(props);

    this.state = {
      question: this.props.edit ? this.props.cardQuestion : "",
      answer: this.props.edit ? this.props.cardAnswer : "",
      id: this.props.edit ? this.props.cardId : "",
      questionError: "",
      answerError: "",
    }

    this.props.navigator.setButtons({
      leftButtons: [],
      rightButtons: [{
          title: 'Save',
          id: 'save',
          buttonFontSize: 16,
          buttonFontWeight: '600',
      }],
      animated: false
    });
    
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  _postCard() {
    return fetch('http://localhost:3000/cards', {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              question: this.state.question,
              answer: this.state.answer,
            })
          })
          .then((response) => response.json())
          .then((responseJson) => {
            if (responseJson.message === "success") {
              this.props.navigator.resetTo({
                screen: 'cardz.CardsList',
                title: 'Cardz',
                passProps: {},
                animated: true,
                animationType: 'slide-horizontal',
              });
            } else {
              this.setState({
                answerError: responseJson.message
              });
            }
          });
  }

  _patchCard() {
    var url = 'http://localhost:3000/cards/' + this.props.cardId;
    return fetch(url, {
            method: 'PATCH',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              question: this.state.question,
              answer: this.state.answer,
            })
          })
          .then((response) => response.json())
          .then((responseJson) => {
            if (responseJson.message === "OK") {
              this.props.navigator.resetTo({
                screen: 'cardz.CardsList',
                title: 'Cardz',
                passProps: {},
                animated: true,
                animationType: 'slide-horizontal',
              });
            } else {
              this.setState({
                answerError: responseJson.message
              });
            }
          });
  }

  onNavigatorEvent(event) {
    if (event.type == 'NavBarButtonPress') {
      if (event.id == 'save') {
        let lastChar = this.state.question.slice(-1);
        if (this.state.question.length > 8 && lastChar === '?') {
          if (this.props.edit) {
            this._patchCard();
          } else {
            this._postCard();
          }
        } else {
          this.setState({
            questionError: "The question has to be longer than 8 characters and end with a question mark"
          })
        }
      }
    }
  }

  _questionTextChanged = (text) => {
    this.setState({
      question: text,
      questionError: ""
    });
  }

  _answerTextChanged = (text) => {
    this.setState({
      answer: text,
      answerError: ""
    });
  }

  render() {
    const questionErrorText = this.state.questionError !== "" ? 
    <Text style={styles.errorMessage}>{this.state.questionError}</Text> : null;

    const answerErrorText = this.state.answerError !== "" ? 
    <Text style={styles.errorMessage}>{this.state.answerError}</Text> : null;

    return (
      <View style={styles.container}>
        
        <TextInput 
        style={styles.textField}
        defaultValue={this.state.question}
        multiline={true}
        placeholder="Question"
        onChangeText={this._questionTextChanged}/>
        {questionErrorText}

        <TextInput
        style={styles.textField}
        defaultValue={this.state.answer}
        multiline={true}
        placeholder="Answer"
        onChangeText={this._answerTextChanged}/>
        {answerErrorText}

      </View>
    );
  }
}

const styles = StyleSheet.create({
  textField: {
    margin: 20,
    padding: 10,
    fontSize: 16,
    backgroundColor: '#ffffff',
    borderBottomWidth: 1,
  },
  errorMessage: {
    marginTop: -10,
    marginLeft: 20,
    marginRight: 20,
    fontSize: 12,
    color: '#ff4500'
  },
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
});
