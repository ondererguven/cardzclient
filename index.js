import { AppRegistry } from 'react-native';
import App from './App';

import { Navigation } from 'react-native-navigation';

//NOTE: Insted of registering an app as in the default template,
// I register the navigator which handles the screen presentation

import { registerScreens } from './screens';

registerScreens();

Navigation.startSingleScreenApp({
    screen: {
      screen: 'cardz.CardsList',
      title: 'Cardz',
      navigatorStyle: {},
      navigatorButtons: {},
    },
    passProps: {},
    animationType: 'slide-down'
});
