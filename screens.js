import { Navigation } from 'react-native-navigation';

import CardsList from './CardsList';
import CardInteractor from './CardInteractor';

// Registering the screens to use inside the app
export function registerScreens() {
  Navigation.registerComponent('cardz.CardsList', () => CardsList);
  Navigation.registerComponent('cardz.CardInteractor', () => CardInteractor);
}