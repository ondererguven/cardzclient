'use strict';

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  FlatList,
  Button,
  TouchableHighlight,
  Dimensions,
} from 'react-native';

// Device screen dimensions
const {height, width} = Dimensions.get('window');
const itemWidth = width - 20; 

// List item class declaration
class CardListItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      textIsQuestion: true,
      text: ""
    };
  }

  _onPress = () => {
    if (this.state.textIsQuestion === true) {
      this.setState({
        textIsQuestion: false,
        text: this.props.item.answer
      })
    } else {
      this.setState({
        textIsQuestion: true,
        text: this.props.item.question
      })
    }
  };

  // Before presenting the item, decide to show the question or the answer
  componentWillMount() {
    if (Math.floor(Math.random() * 101) % 2 === 0) {
      this.setState({
        textIsQuestion: true,
        text: this.props.item.question
      })
    } else {
      this.setState({
        textIsQuestion: false,
        text: this.props.item.answer
      })
    }
  }

  _onPressEditButton = () => {
    this.props.onPressItem(this.props.item.question, this.props.item.answer, this.props.item._id);
  }

  render() {
    return (
      <TouchableHighlight
      onPress={this._onPress}
      underlayColor='#ffffff'>
        <View>
          <View style={styles.rowContainer}>

            <View style={styles.textContainer}>

              <Text style={styles.cardText}>
              {this.state.text}
              </Text>

            </View>
            
            <TouchableHighlight 
            onPress={this._onPressEditButton}
            underlayColor='#ffffff'>
              <Text style={styles.editButton}> Edit </Text>
            </TouchableHighlight>

          </View>

        </View>
      </TouchableHighlight>
    );
  }
}

// Screen class declaration
export default class CardsList extends Component<{}> {
  constructor(props) {
    super(props);

    this.state = {
      cards: []
    };

    // Add the create button to the nav bar, comes with react-native-navigation dependency
    this.props.navigator.setButtons({
      leftButtons: [],
      rightButtons: [{
          title: 'Create',
          id: 'create',
          buttonFontSize: 16,
          buttonFontWeight: '600',
      }],
      animated: true
    });

    // bind the create button to a method
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  componentDidMount() {
    this._getCards();
  }

  _getCards() {
    return fetch('http://localhost:3000/cards')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          cards: responseJson.data,
        });
      })
      .catch((error) => {
        console.error(error);
      });
  }

  _keyExtractor = (item, index) => index;

  _renderItem = ({item, index}) => (
      <CardListItem
          item = {item}
          index = {index}
          onPressItem = {this._onPressItem}
      />
  );

  onNavigatorEvent(event) {
    if (event.type == 'NavBarButtonPress') {
      if (event.id == 'create') {
        this.props.navigator.push({
          screen: 'cardz.CardInteractor',
          title: 'Create',
          passProps: {
            cards: this.state.cards
          },
          backButtonTitle: "",
          animated: true,
          animationType: 'slide-horizontal',
        });
      }
    }
  }

  _onPressItem = (question, answer, _id) => {
    this.props.navigator.push({
      screen: 'cardz.CardInteractor',
      title: 'Edit',
      passProps: {
        cardId: _id,
        cardQuestion: question,
        cardAnswer: answer,
        edit: true // pass as a prop to check if mode is edit or create in interactor
      },
      backButtonTitle: "",
      animated: true,
      animationType: 'slide-horizontal',
    });
  };

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#ffffff'}}>
        <FlatList
          contentContainerStyle = {styles.listStyle}
          horizontal = {true}
          showsHorizontalScrollIndicator = {false}
          data = {this.state.cards}
          keyExtractor = {this._keyExtractor}
          renderItem = {this._renderItem}
          extraData = {this.state.cards}
          snapToAlignment = {'center'}
          pagingEnabled = {true}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  listStyle: {
    alignItems: 'center',
  },
  editButton: {
    fontSize: 14,
    color: '#007AFF',
  },
	textContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginLeft: 5,
    marginRight: 5,
    marginTop: 5,
    marginBottom: 5,
  },
  cardText: {
    fontSize: 20,
    color: '#000000'
	},
	rowContainer: {
    alignItems: 'center',
    aspectRatio: 1,
    height: itemWidth,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.3,
    shadowRadius: 6,
    elevation: 1,
    backgroundColor: '#ffffff',
    borderRadius: 12,
    marginLeft: 10,
    marginBottom: 30,
    marginRight: 10,
  },
});
