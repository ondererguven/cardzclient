import React, {Component} from 'react';

// card model in case needed inside the app
export default class Card extends Component {
    question: string;
    answer: string;
}